package com.reinertisa.opsysmonitor.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/*
 * 
 * To insert operating system variables like CPU usage and memory usage, I need a MODEL class to create an object
 * I created OperatingSystem model class. This class has id(primary key), cpuUsage and memoryUsage
 * All of them are integer. 
 * After I get operating system variables, 
 * I created OperatingSystem object that contains cpuUsage and memoryUsage using constructor.
 * 
 */

@Entity
public class OperatingSystem {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	int id;
	int cpuUsage;
	int memoryUsage;
	
	public OperatingSystem() {
		
	}

	public OperatingSystem(int cpuUsage, int memoryUsage) {
		super();
		this.cpuUsage = cpuUsage;
		this.memoryUsage = memoryUsage;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCpuUsage() {
		return cpuUsage;
	}

	public void setCpuUsage(int cpuUsage) {
		this.cpuUsage = cpuUsage;
	}

	public int getMemoryUsage() {
		return memoryUsage;
	}

	public void setMemoryUsage(int memoryUsage) {
		this.memoryUsage = memoryUsage;
	}

	@Override
	public String toString() {
		return "OperatingSystem [id=" + id + ", cpuUsage=" + cpuUsage + ", memoryUsage=" + memoryUsage + "]";
	}
	
	

}
