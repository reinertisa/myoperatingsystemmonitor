package com.reinertisa.opsysmonitor.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.reinertisa.opsysmonitor.model.OperatingSystem;
import com.reinertisa.opsysmonitor.repository.OperatingSystemRepository;

/*
 * 
 * This is controller class to handle requests coming from the client
 * In this project ReactJs wants some data from back-end to show on the chart.
 * These data are stored in H2 database. To fetch these data I created List<OperatingSystem> getOperatingSystem() in my controller.
 * These method invokes repository interface. 
 * @Autowired annotations helps use to create object to get all data using this.operatingSystemRepository.findAll();
 * 
 */

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("api/")
public class OperatingSystemController {
	
	@Autowired
	private OperatingSystemRepository operatingSystemRepository;
	
	@GetMapping("operatingsystem")
	public List<OperatingSystem> getOperatingSystem() {
		return this.operatingSystemRepository.findAll();
	}

}