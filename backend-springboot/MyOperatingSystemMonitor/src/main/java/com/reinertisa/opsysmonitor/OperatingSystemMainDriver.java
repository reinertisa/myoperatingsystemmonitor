package com.reinertisa.opsysmonitor;

import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.reinertisa.opsysmonitor.model.OperatingSystem;
import com.reinertisa.opsysmonitor.repository.OperatingSystemRepository;


/*
 * In OperatingSystemMainDriver class, to get "recent CPU usage" for the whole system
 * and to get "recent memory usage" for the whole system I used OperatingSystemMXBean Interface.
 * I created the object returned by ManagementFactory.getOperatingSystemMXBean() is 
 * an is an instance of the implementation class of this interface 
 * or UnixOperatingSystemMXBean interface depending on its underlying operating system.
 * 
 * I need two data to monitor operating system for this challenge. 
 * One of them is recent CPU usage other one is recent memory usage.
 * After I fetch these data. I store them H2 database. 
 * 
 * OperatingSystemRepository operatingSystemRepository helped me to save these data in H2 database.
 * Spring Boot makes it easy to access any database.
 * 
 */


@SpringBootApplication
public class OperatingSystemMainDriver implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(OperatingSystemMainDriver.class, args);
	}

	@Autowired
	private OperatingSystemRepository operatingSystemRepository;
	
	@Override
	public void run(String... args) throws Exception {
		
		int cpuUsageValues = 0;
		int memoryUsageValues = 0;
		double freeMemorySize = 0.0;
		double totalMemorySize = 0.0;
		
		
		OperatingSystemMXBean operatingSystemMXBean = ManagementFactory.getOperatingSystemMXBean();
		int i=0;
		
		//I created 60 loops. if we want, we can create infinite loop while(true) to get operating systems data
		while(i <= 60) {

			for (Method method : operatingSystemMXBean.getClass().getDeclaredMethods()) {
				method.setAccessible(true);
				
				if (method.getName().startsWith("get") && Modifier.isPublic(method.getModifiers())) {
		            Object value;
		            
		            try {
		            	value = method.invoke(operatingSystemMXBean);
		            } catch (Exception e) {
		            	value = e;
		            } 
		        
		            // I get the recent CPU usage for the whole system
		            if("getSystemCpuLoad".equals(method.getName())) {
		            	cpuUsageValues = (int)(Double.parseDouble(value.toString())*100);
		            }
		        	
		            // I get the free physical memory size
		            if("getFreePhysicalMemorySize".equals(method.getName())) {
		            	freeMemorySize = Double.parseDouble(value.toString());	  
		            }
		        
		            // I get the total physical memory size
		            if("getTotalPhysicalMemorySize".equals(method.getName())) {
		            	totalMemorySize = Double.parseDouble(value.toString());
		            }
				} 
			} 
			
			//I wait 1 second here and then I add CPU and memory usage in H2 database
			try{
                 Thread.sleep(1000);     
			} catch (InterruptedException e) {
            	 e.printStackTrace();
            }
			

		
			//I calculate the recent memory usage here
			memoryUsageValues = (int)(((totalMemorySize - freeMemorySize) / totalMemorySize) * 100);	
			
			//I insert CPU usage and memory usage in H2 database.
			//new OperatingSystem(cpuUsageValues, memoryUsageValues is an object using my OperatingSystem model class.
			//First I get CPU usage and memory usage, second I add them in my object and finally 
			//I insert them in H2 database using my repository interface
			this.operatingSystemRepository.save(new OperatingSystem(cpuUsageValues, memoryUsageValues));
			
			i++;
		}
		
	}

}


