package com.reinertisa.opsysmonitor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.reinertisa.opsysmonitor.model.OperatingSystem;

/*
 * 
 * This is my Repository interface extends Spring Boot JPA to manage relational data in Java applications.
 * It allows us to access and persist data between Java object/class and relational database
 * If you want you can declare custom methods but most of them are ready and you can just use them.
 * 
 */

@Repository
public interface OperatingSystemRepository extends JpaRepository<OperatingSystem, Integer>{

}
