import axios from 'axios'

/* I imported axios(third party HTTP library) to make HTTP call or REST API call*/

/*CPU_REST_API_URL keeps where the JSON format data in H2 database */
const CPU_REST_API_URL = 'http://localhost:8080/api/operatingsystem';

/*OpSys service class get all CPUInfo data in H2 database using axios http library*/ 
class OpSysService{

    getCPUInfo(){
       return axios.get(CPU_REST_API_URL);
    }
}

export default new OpSysService();