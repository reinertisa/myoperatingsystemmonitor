import React from 'react';
import './App.css';
import OpSysComponent from './components/OpSysComponent';

function App() {
  return (
    <div className="App">
      <OpSysComponent></OpSysComponent>
    </div>
  );
}

export default App;