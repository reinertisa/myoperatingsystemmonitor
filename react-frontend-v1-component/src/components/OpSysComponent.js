import React, { Fragment } from 'react';
import OpSysService from '../services/OpSysService';
import Plotly from "plotly.js-basic-dist";
import createPlotlyComponent from "react-plotly.js/factory";
const Plot = createPlotlyComponent(Plotly);

/*
 I imported Plotly to create chart and OpSysService to fetch data from H2 database
 our data will return JSON format
*/

class OpSysComponent extends React.Component{
    
    constructor(probs){
        super(probs);
        this.state = {
            timeChartXValues : [], // it will keep all primary key
            cpuUsageChartYValues : [], // it will kepp all cpuUsage data 
            memoryUsageChartYValues : [], //it will keep all memoryUsage data
            value : "CPU" // default our radio button value is CPU
        }
    }

    //Component Lifecycle Method -  is executed after the first render only on the client side
    componentDidMount(){
        this.myInterval = setInterval(() => {
            this.getAllCPUInfos();
        }, 5000);
    }

    componentWillUnmount(){
        clearInterval(this.myInterval);
    } 

    //this functions fetch all data from H2 database
    getAllCPUInfos(){

        const pointerTothis = this;
        let timeChartXValuesFunction = [];
        let cpuUsageChartYValuesFunction = [];
        let memoryUsageChartYValuesFunction = [];

 
        OpSysService.getCPUInfo() // OpSysService returns all data from H2 database in JSON format
        .then(response => {
           
            for(const dataObj of response.data){ // we parse all data using for loop and add functions
                timeChartXValuesFunction.push(parseInt(dataObj.id)-1)
                cpuUsageChartYValuesFunction.push(parseInt(dataObj.cpuUsage)/100)
                memoryUsageChartYValuesFunction.push(parseInt(dataObj.memoryUsage)/100)
            }

            pointerTothis.setState({ // we 
                timeChartXValues : timeChartXValuesFunction,
                cpuUsageChartYValues : cpuUsageChartYValuesFunction,
                memoryUsageChartYValues : memoryUsageChartYValuesFunction
            })

        });  
    }

    render(){
        return (
          
            <Fragment>
                <section>
                    {this.renderSelecter()} 
                    <div>
                        {this.renderSelected(this.state.value)}
                    </div>
                </section>
            </Fragment>
           
        );
    }
   
    //After we selected CPU or Memory using radio botton, we updated value to show the related chart
    onChange = e =>{
        this.setState({value : e.target.value})
    }

    // we can select CPU or Memory using Radio button
    renderSelecter(){
        const {value} = this.state;
        return(
            <div>
                <form>
                    <label>
                        CPU
                        <input type="radio" 
                               value="CPU"
                               checked={value === "CPU"}
                               onChange={this.onChange}/>
                    </label>

                    <label>
                        Memory
                        <input type="radio" 
                               value="Memory"
                               checked={value === "Memory"}
                               onChange={this.onChange}/>
                    </label>
                </form>
            </div>
        )
    }

    //After we selected CPU or Memory using radio button, I used if and else to show related chart
    renderSelected(value){
   
        if(value === "CPU"){
            return(
                <div>
                    {this.showCPUUsage()}
                </div>
            )
        } else {
            return(
                <div>
                    {this.showMemoryUsage()}
                </div>
            )
        }
        
    }

    //this function show CPU Usage chart using Ploty
    showCPUUsage(){

        return(
            <div>
                <Plot
                     data={[
                            {
                                x: this.state.timeChartXValues,
                                y: this.state.cpuUsageChartYValues,
                                
                                type: 'scatter',
                                mode: 'lines',
                                marker: {color: 'red'},
                                fill: 'tozeroy',
                            }
                        ]}
                        
                        layout={
                            {
                                width: 1000, 
                                height: 440, 
                                title: 'Operating System Monitor',
                                
                                xaxis : {
                                    title : 'Time',
                                    range : [60,0],
                                },
                                
                                yaxis :{
                                    title : 'CPU Usage',
                                    tickformat : ',.0%',
                                    range : [0,1],
                                },                     
                            }      
                        }   
                        />

            </div>
        );
    }

    //this function show Memory Usage chart using Ploty
    showMemoryUsage(){

        return(
            <div>
                <Plot
                     data={[
                            {
                                x: this.state.timeChartXValues,
                                y: this.state.memoryUsageChartYValues,
                                
                                type: 'scatter',
                                mode: 'lines',
                                marker: {color: 'red'},
                                fill: 'tozeroy',
                            }
                        ]}
                        
                        layout={
                            {
                                width: 1000, 
                                height: 440, 
                                title: 'Operating System Monitor',
                                
                                xaxis : {
                                    title : 'Time',
                                    range : [60,0],
                                },
                                
                                yaxis :{
                                    title : 'Memory Usage',
                                    tickformat : ',.0%',
                                    range : [0,1],
                                },                     
                            }      
                        }   
                        />

            </div>
        );
    }

}

export default OpSysComponent;