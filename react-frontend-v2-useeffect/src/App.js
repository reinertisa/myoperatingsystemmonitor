import React, { useEffect, useState} from 'react';
import axios from 'axios';
import './App.css';
import Plotly from "plotly.js-basic-dist";
import createPlotlyComponent from "react-plotly.js/factory";
const Plot = createPlotlyComponent(Plotly);


const useFetch = (url) => {

    const [xValues, setXValues] = useState({timeChartXValues : []});
    const [cpuValues, setCpuValues] = useState({cpuUsageChartYValues : []});
    const [memoryValues, setMemoryValues] = useState({memoryUsageChartYValues : []});
    const [flag, setFlag] = useState(true);
    const [timer, setTimer] = useState(false);
    const [loading, setLoading] = useState(true);
    

    useEffect(()=>{
        setTimer(true)
    }, []);


    useEffect(()=>{
    
        setInterval(() => {
            if(timer){
                setFlag(false);
            } else {
                setFlag(true);
            }
        }, 5000);       
        
    }, [timer]);

    useEffect(()=>{

    }, [loading]);


    useEffect(async () =>{
        if(loading){
            
            let timeChartXValuesFunction = [];
            let cpuUsageChartYValuesFunction = [];
            let memoryUsageChartYValuesFunction = [];

            const response = axios.get(url);
            let i = 0;
            for(const dataObj of (await response).data){
                if(i === 0){
                    timeChartXValuesFunction.push(0)
                    cpuUsageChartYValuesFunction.push(0)    
                    memoryUsageChartYValuesFunction.push(0)
                } else {
                    timeChartXValuesFunction.push(parseInt(dataObj.id))
                    cpuUsageChartYValuesFunction.push(parseInt(dataObj.cpuUsage)/100)    
                    memoryUsageChartYValuesFunction.push(parseInt(dataObj.memoryUsage)/100)
                }

            if(i === 60){
                break;
            }
            i++;
        }
        
        if(i === 60){
            setLoading(false);
        } else {
            setTimer(true);
        }

        setXValues({timeChartXValues : timeChartXValuesFunction});
        setCpuValues({cpuUsageChartYValues : cpuUsageChartYValuesFunction});
        setMemoryValues({memoryUsageChartYValues : memoryUsageChartYValuesFunction});
        
        }
    }, [flag]);

    return {xValues, cpuValues, memoryValues, loading};
}


export default function App() {

    const [value, setValue] = useState("CPU"); 
    const {xValues, cpuValues, memoryValues, loading} = useFetch("http://localhost:8080/api/operatingsystem");   
  
    function selection(){
        return(
         <>
          <div> 
            <form>
                <label>
                    CPU
                    <input type = "radio" value = "CPU" checked={value === "CPU"} onChange={onChange}/>
                </label>
                <br></br>
                <label>
                    Memory
                    <input type="radio" value = "Memory" checked = {value === "Memory"} onChange = {onChange}/>
                </label>
            </form>
         </div>
          </>
        )

    }

//After we selected CPU or Memory using radio botton, we updated value to show the related chart
    function onChange(e){    
        setValue(e.target.value);        
    }

//this function show CPU Usage chart using Ploty
    function showCPUUsage(){
        
        return(            
            <div>
                 <Plot
                    data={[
                        {
                            x: xValues.timeChartXValues,
                            y: cpuValues.cpuUsageChartYValues,
                          
                            type: 'scatter',
                            mode: 'lines',
                            marker: {color: 'red'},
                            fill: 'tozeroy',
                        }
                    ]}
                  
                    layout={
                        {
                            width: 1000, 
                            height: 440, 
                            title: 'Operating System Monitor',
                          
                            xaxis : {
                                title : 'Time',
                                range : [60,0],
                            },
                          
                            yaxis :{
                                title : 'CPU Usage',
                                tickformat : ',.0%',
                                range : [0,1],
                            },                     
                        }      
                    }   
                  />
            </div>
            
        );
    }

//this function show Memory Usage chart using Ploty
    function showMemoryUsage(){
        return(
            <div>
                <Plot
                    data={[
                        {
                            x: xValues.timeChartXValues,
                            y: memoryValues.memoryUsageChartYValues,
                          
                            type: 'scatter',
                            mode: 'lines',
                            marker: {color: 'red'},
                            fill: 'tozeroy',
                        }
                    ]}
                  
                    layout={
                        {
                            width: 1000, 
                            height: 440, 
                            title: 'Operating System Monitor',
                          
                            xaxis : {
                                title : 'Time',
                                range : [60,0],
                            },
                          
                            yaxis :{
                                title : 'Memory Usage',
                                tickformat : ',.0%',
                                range : [0,1],
                            },                     
                        }      
                    }   
                />
            </div>
        );
    }

    return (
             
            <div className ="App">
                { selection() }
                {value === "CPU" ? <div>{showCPUUsage()}</div> : <div>{showMemoryUsage()}</div>}
            </div>
       
    )
  
}
